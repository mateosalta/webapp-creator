FROM ubuntu:xenial
MAINTAINER Mateo Salta

RUN apt-get update && apt-get install -y software-properties-common

RUN add-apt-repository ppa:bhdouglass/clickable

RUN apt-get update && \
    apt-get install -y clickable && \
    apt-get -y install sudo && \
    apt-get -y install cmake && \
    sudo apt-get install -y build-essential
    
RUN apt-get -y --force-yes --no-install-recommends install \
    apt-utils \
    build-essential \
    cmake \
    dpkg-cross \
    fakeroot \
    libc-dev \
    isc-dhcp-client \
    net-tools \
    ifupdown \
    g++-arm-linux-gnueabihf \
    pkg-config-arm-linux-gnueabihf \
    intltool \
    oxideqt-codecs-extra \
    qt5-doc \
    language-pack-en \
    click \
    qtbase5-private-dev \
    qtdeclarative5-private-dev \
    libqt5opengl5-dev \
    mercurial \
    crossbuild-essential-armhf \
    git \
    libicu-dev \
    qtfeedback5-dev \
qtsystems5-dev
    
RUN apt-get install -y wget
RUN apt-get install -y click-reviewers-tools 

    
SHELL ["/bin/bash", "-c"]
    
    
RUN useradd -m docker -g sudo && echo "docker:docker" | chpasswd && adduser docker sudo

USER root


RUN clickable setup-docker

COPY . /home/docker/build


RUN cd /home/docker/build/webapp-creator && clickable --container

